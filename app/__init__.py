from flask import Flask, jsonify, render_template

app = Flask(__name__, template_folder='static')

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/<nome>')
def ola(nome):
    return render_template(
        'index.html',
        nome=nome
    )

"""
@app.route('/<usuario>')
def ola_usuario(usuario):
    return f'Olá, {usuario}'
"""


@app.route('/cafe/<cafe>')
def cafe(cafe):
    return f'Eu prefiro o café {cafe}'


@app.route('/pessoa/<nome>')
def pessoa(nome):
    r = {
        'nome': nome,
        'idade': 18
    }

    return jsonify(r)